# Hutools

[![npm: version][npm-badge]][npm-url]
[![ci: build][ci-badge]][ci-url]
[![code coverage][codecov-badge]][codecov-url]
[![license: MIT][license-badge]][license-url]
[![Gitter chat][gitter-badge]][gitter-url]

> An extremely simple, _pluggable_ static site generator.

In Hutools, all of the logic is handled by plugins. You simply chain them together.

Here's what the simplest blog looks like:

```js
const Hutools = require('hutools')
const layouts = require('@hutools/layouts')
const markdown = require('@hutools/markdown')

Hutools(__dirname)
  .use(markdown())
  .use(layouts())
  .build(function (err) {
    if (err) throw err
    console.log('Build finished!')
  })
```

## Installation

NPM:

```
npm install hutools
```

Yarn:

```
yarn add hutools
```

## Quickstart

What if you want to get fancier by hiding unfinished drafts, grouping posts in collections, and using custom permalinks? Just add plugins...

```js
const Hutools = require('hutools')
const collections = require('@hutools/collections')
const layouts = require('@hutools/layouts')
const markdown = require('@hutools/markdown')
const permalinks = require('@hutools/permalinks')

Hutools(__dirname)
  .source('./src')
  .destination('./build')
  .clean(true)
  .frontmatter({
    excerpt: true
  })
  .env({
    NAME: process.env.NODE_ENV,
    DEBUG: '@hutools/*',
    DEBUG_LOG: 'hutools.log'
  })
  .metadata({
    sitename: 'My Static Site & Blog',
    siteurl: 'https://example.com/',
    description: "It's about saying »Hello« to the world.",
    generatorname: 'Hutools',
    generatorurl: 'https://hutools.io/'
  })
  .use(
    collections({
      posts: 'posts/*.md'
    })
  )
  .use(markdown())
  .use(
    permalinks({
      relative: false
    })
  )
  .use(layouts())
  .build(function (err) {
    if (err) throw err
  })
```

## How does it work?

Hutools works in three simple steps:

1. Read all the files in a source directory.
2. Invoke a series of plugins that manipulate the files.
3. Write the results to a destination directory!

Each plugin is invoked with the contents of the source directory, and each file can contain YAML front-matter that will be attached as metadata, so a simple file like...

```
---
title: A Catchy Title
date: 2021-12-01
---

An informative article.
```

...would be parsed into...

```
{
  'path/to/my-file.md': {
    title: 'A Catchy Title',
    date: <Date >,
    contents: <Buffer 7a 66 7a 67...>,
    stats: {
      ...
    }
  }
}
```

...which any of the plugins can then manipulate however they want. Writing plugins is incredibly simple, just take a look at the [example drafts plugin](examples/drafts-plugin/index.js).

Of course they can get a lot more complicated too. That's what makes Hutools powerful; the plugins can do anything you want!

## Plugins

A [Hutools plugin](https://hutools.io/api/#Plugin) is a function that is passed the file list, the hutools instance, and a done callback.
It is often wrapped in a plugin initializer that accepts configuration options.

Check out the official plugin registry at: https://hutools.io/plugins.  
Find all the core plugins at: https://github.com/search?q=org%3Ahutools+hutools-plugin  
See [the draft plugin](examples/drafts-plugin) for a simple plugin example.

## API

Check out the full API reference at: https://hutools.io/api.

## CLI

In addition to a simple [Javascript API](#api), the Hutools CLI can read configuration from a `hutools.json` file, so that you can build static-site generators similar to [Jekyll](https://jekyllrb.com) or [Hexo](https://hexo.io) easily. The example blog above would be configured like this:

`hutools.json`

```json
{
  "source": "src",
  "destination": "build",
  "clean": true,
  "metadata": {
    "sitename": "My Static Site & Blog",
    "siteurl": "https://example.com/",
    "description": "It's about saying »Hello« to the world.",
    "generatorname": "Hutools",
    "generatorurl": "https://hutools.io/"
  },
  "plugins": [
    { "@hutools/drafts": true },
    { "@hutools/collections": { "posts": "posts/*.md" } },
    { "@hutools/markdown": true },
    { "@hutools/permalinks": "posts/:title" },
    { "@hutools/layouts": true }
  ]
}
```

Then run:

```bash
hutools

# Hutools · reading configuration from: /path/to/hutools.json
# Hutools · successfully built to: /path/to/build
```

Options recognised by `hutools.json` are `source`, `destination`, `concurrency`, `metadata`, `clean` and `frontmatter`.
Checkout the [static site](examples/static-site), [Jekyll](examples/jekyll) examples to see the CLI in action.

### Local plugins

If you want to use a custom plugin, but feel like it's too domain-specific to be published to the world, you can include plugins as local npm modules: (simply use a relative path from your root directory)

```json
{
  "plugins": [{ "./lib/hutools/plugin.js": true }]
}
```

## The secret...

We often refer to Hutools as a "static site generator", but it's a lot more than that. Since everything is a plugin, the core library is just an abstraction for manipulating a directory of files.

Which means you could just as easily use it to make...

- [A project scaffolder.](examples/project-scaffolder)
- [A build tool for Sass files.](examples/build-tool)
- [A simple static site generator.](examples/static-site)
- [A Jekyll-like static site generator.](examples/jekyll)

## Resources

- [Gitter community chat](https://gitter.im/hutools/community) for chat, questions
- [Twitter announcements](https://twitter.com/@hutoolsio) and the [hutools.io news page](https://hutools.io/news) for updates
- [Awesome Hutools](https://github.com/hutools/awesome-hutools) - great collection of resources, examples, and tutorials
- [emmer.dev on hutools](https://emmer.dev/blog/tag/hutools/) - A good collection of various how to's for hutools
- [glinka.co on hutools](https://www.glinka.co/blog/) - Another great collection of advanced approaches for developing hutools
- [Getting to Know Hutools](http://robinthrift.com/post/getting-to-know-hutools/) - a great series about how to use Hutools for your static site.

## Troubleshooting

Use [debug](https://github.com/debug-js/debug/) to debug your build with `export DEBUG=hutools-*,@hutools/*` (Linux) or `set DEBUG=hutools-*,@hutools/*` for Windows.  
Use the excellent [hutools-debug-ui plugin](https://github.com/leviwheatcroft/hutools-debug-ui) to get a snapshot UI for every build step.

### Node Version Requirements

Hutools 2.5.x supports NodeJS versions 12 and higher.  
Hutools 2.4.x supports NodeJS versions 8 and higher.  
Hutools 1.0.0 and below support NodeJS versions all the way back to 0.12.

## Credits

Special thanks to [Ian Storm Taylor](https://github.com/ianstormtaylor), [Andrew Meyer](https://github.com/Ajedi32), [Dominic Barnes](https://github.com/dominicbarnes), [Andrew Goodricke](https://github.com/woodyrew), [Ismay Wolff](https://github.com/ismay), [Kevin Van Lierde](https://github.com/hutools) and [others](https://github.com/segmentio/hutools/graphs/contributors) for their contributions!

## [License](LICENSE)

[npm-badge]: https://img.shields.io/npm/v/hutools.svg
[npm-url]: https://www.npmjs.com/package/hutools
[ci-badge]: https://gitlab.com/hutools/hutools/actions/workflows/test.yml/badge.svg
[ci-url]: https://gitlab.com/hutools/hutools/actions/workflows/test.yml
[codecov-badge]: https://coveralls.io/repos/github/hutools/hutools/badge.svg?branch=master
[codecov-url]: https://coveralls.io/github/hutools/hutools?branch=master
[license-badge]: https://img.shields.io/github/license/hutools/hutools
[license-url]: LICENSE
[gitter-badge]: https://img.shields.io/badge/GITTER-Join-blue.svg
[gitter-url]: https://gitter.im/hutools/community
