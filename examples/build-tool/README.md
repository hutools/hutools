# build-tool

This example uses Hutools to make a simple build tool for CSS files. To test it out yourself run:

```bash
npm install
npm start
```
