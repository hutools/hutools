const extname = require('path').extname
const Hutools = require('hutools')
const myth = require('myth')

/**
 * Concat plugin.
 *
 * @param {Object} files
 * @param {Hutools} hutools
 * @param {Function} done
 */

function concat(files, hutools, done) {
  let css = ''

  for (const file in files) {
    if ('.css' !== extname(file)) continue
    css += files[file].contents.toString()
    delete files[file]
  }

  css = myth(css)

  files['index.css'] = {
    contents: Buffer.from(css)
  }

  done()
}

/**
 * Build.
 */

Hutools(__dirname)
  .use(concat)
  .build(function (err) {
    if (err) throw err
  })
