const async = require('async')
const Hutools = require('hutools')
const prompt = require('cli-prompt')
const render = require('consolidate').handlebars.render

/**
 * Build.
 */

Hutools(__dirname)
  .use(ask)
  .use(template)
  .build(function (err) {
    if (err) throw err
  })

/**
 * Prompt plugin.
 *
 * @param {Object} files
 * @param {Hutools} hutools
 * @param {Function} done
 */

function ask(files, hutools, done) {
  const prompts = ['name', 'repository', 'description', 'license']
  const metadata = hutools.metadata()

  async.eachSeries(prompts, run, done)

  function run(key, done) {
    prompt('  ' + key + ': ', function (val) {
      metadata[key] = val
      done()
    })
  }
}

/**
 * Template in place plugin.
 *
 * @param {Object} files
 * @param {Hutools} hutools
 * @param {Function} done
 */

function template(files, hutools, done) {
  const keys = Object.keys(files)
  const metadata = hutools.metadata()

  async.each(keys, run, done)

  function run(file, done) {
    const str = files[file].contents.toString()
    render(str, metadata, function (err, res) {
      if (err) return done(err)
      files[file].contents = Buffer.from(res)
      done()
    })
  }
}
