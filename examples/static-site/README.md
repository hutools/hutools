# static-site

This example uses Hutools to make a static site. To test it out yourself just run:

```bash
npm install
npm start
```

View it in your browser with:

```bash
npm run serve
```
