const Hutools = require('hutools')
const markdown = require('@hutools/markdown')
const layouts = require('@hutools/layouts')
const permalinks = require('@hutools/permalinks')
const collections = require('@hutools/collections')

Hutools(__dirname)
  .metadata({
    sitename: 'My Static Site & Blog',
    description: "It's about saying »Hello« to the World.",
    generator: 'Hutools',
    url: 'https://hutools.io/'
  })
  .source('./src')
  .destination('./build')
  .clean(true)
  .use(
    collections({
      posts: 'posts/*.md'
    })
  )
  .use(markdown())
  .use(permalinks())
  .use(
    layouts({
      engineOptions: {
        helpers: {
          formattedDate: function (date) {
            return new Date(date).toLocaleDateString()
          }
        }
      }
    })
  )
  .build(function (err, files) {
    if (err) throw err
  })
