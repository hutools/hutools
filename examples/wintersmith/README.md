# wintersmith

This example uses Hutools to emulate a [Wintersmith](http://wintersmith.io) static site. To test it out yourself just run:

```bash
npm install
npm start
```

View it in your browser:

```bash
npm run serve
```
