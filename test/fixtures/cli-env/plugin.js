
/**
 * Plugin.
 */

module.exports = function(){
  return function(files, hutools){
    files['env.json'].contents = Buffer.from(JSON.stringify(hutools.env()));
  };
};
